package Zuoye1;

public class Student {
	private String stuNumber;
	private int age;
	private String name;
	private String IDNumber;

	public Student() {
	}

	public Student(String stuNumber, int age, String name, String iDNumber) {
		super();
		this.stuNumber = stuNumber;
		this.age = age;
		this.name = name;
		IDNumber = iDNumber;
	}

	public Student(String stuNumber, String name) {
		super();
		this.stuNumber = stuNumber;
		this.name = name;
	}

	public void setStuNumber(String stuNumber) {
		this.stuNumber = stuNumber;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIDNumber(String iDNumber) {
		IDNumber = iDNumber;
	}

	public String getStuNumber() {
		return stuNumber;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	public String getIDNumber() {
		return IDNumber;
	}

	@Override
	public String toString() {
		return "Students [stuNumber=" + stuNumber + ", age=" + age + ", name="
				+ name + ", IDNumber=" + IDNumber + "]";
	}

	private void sayHi(String to) {
		System.out.println(this.name + "��" + to + "�ʺ�����ã�");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((stuNumber == null) ? 0 : stuNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (stuNumber == null) {
			if (other.stuNumber != null)
				return false;
		} else if (!stuNumber.equals(other.stuNumber))
			return false;
		return true;
	}
}
