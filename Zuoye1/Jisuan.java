package Zuoye1;

import java.util.*;
import java.io.*;

public class Jisuan {

	public static void main(String[] args) throws Exception {
		Scanner input = new Scanner(System.in);
		System.out.println("请输入文件名：");
		String FileName = input.next();
		// 创建输入字符流和缓冲区对象

		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(FileName);
			br = new BufferedReader(fr);
			int num = 0;
			// 每次读取一行，并且行数加1；

			while (br.readLine() != null) {
				num++;
			}
			System.out.println("该文件一共有" + num + "行");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
