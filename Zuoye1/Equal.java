package Zuoye1;

import java.util.*;
import java.io.*;

public class Equal {

	public static void main(String[] args) {
		FileReader fr = null;
		BufferedReader br = null;
		List<Integer> list = new ArrayList<Integer>();
		try {
			fr = new FileReader("d:/data1.txt");
			br = new BufferedReader(fr);
			String num = null;
			while ((num = br.readLine()) != null) {
				list.add(Integer.valueOf(num));
			}
			int Max = getMax(list);
			System.out.println(Max);
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}

	}

	private static int getMax(List<Integer> list) {
		int Max = list.get(0);
		Iterator<Integer> it = list.iterator();
		while (it.hasNext()) {
			if (Max < it.next()) {
				Max = it.next();
			}
		}
		return Max;
	}
}
