package Zuoye1;

import java.io.File;
import java.util.Scanner;

public class Xiangtong {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("请输入第一个文件名：");
		String firstName = input.next();
		System.out.println("请输入第二个文件名：");
		String secondName = input.next();
		File file1 = new File(firstName);
		File file2 = new File(secondName);
		String name1 = file1.getName();
		String name2 = file2.getName();
		if (name1.equals(name2)) {
			System.out.println("文件名相同");
		} else {
			System.out.println("文件名不同");
		}

	}

}
